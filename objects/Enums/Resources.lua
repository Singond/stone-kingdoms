---@enum resource
local RESOURCES = {
    wood = "wood",
    stone = "stone",
    wheat = "wheat",
    iron = "iron",
    flour = "flour",
    hop = "hop",
    tar = "tar",
    ale = "ale",
}

return RESOURCES
